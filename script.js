db.users.find(
    {$or: [
        {"firstName": { $regex: "a", $options: "i"}},
        {"lastName": { $regex: "a", $options: "i"}}
    ]},
    {
        "_id":0,
		"firstName":1,
        "lastName":1
	}
)


db.users.find(
    {$and: [
        {"isAdmin":"yes"},
        {"isActive": "yes"}
    ]}
)


db.courses.find(
    {$and: [
        {"name": { $regex: "u", $options: "i"}},
        {"price":{$gt:"13000"}}
    ]}
)